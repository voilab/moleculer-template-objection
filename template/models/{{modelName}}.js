const { Model } = require('objection');

class {{modelName}} extends Model {
    static get tableName() {
        return '{{serviceName}}';
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }
}

module.exports = {{modelName}};
