module.exports = {
{{#if_eq databaseEngine "crdb"}}
    client: 'pg',
    version: '12',
    connection: {
        host: process.env.PG_HOST || /* istanbul ignore next */ 'localhost',
        port: process.env.PG_PORT || /* istanbul ignore next */ '26257',
        user: process.env.PG_USER || /* istanbul ignore next */ 'root',
        password: process.env.PG_PASSWORD || /* istanbul ignore next */ '',
        database: process.env.PG_DATABASE || /* istanbul ignore next */ ''
    },
{{/if_eq}}
{{#if_eq databaseEngine "pg"}}
    client: 'pg',
    connection: {
        host: process.env.PG_HOST || /* istanbul ignore next */ 'localhost',
        port: process.env.PG_PORT || /* istanbul ignore next */ '5432',
        user: process.env.PG_USER || /* istanbul ignore next */ 'root',
        password: process.env.PG_PASSWORD || /* istanbul ignore next */ '',
        database: process.env.PG_DATABASE || /* istanbul ignore next */ ''
    },
{{/if_eq}}
{{#if_eq databaseEngine "sqlite"}}
    client: 'sqlite',
    connection: {
        filename: process.env.SQLITE_FILE || /* istanbul ignore next */ './db.sqlite'
    },
{{/if_eq}}
{{#if_eq databaseEngine "mysql"}}
    client: 'mysql',
    connection: {
        host: process.env.MYSQL_HOST || /* istanbul ignore next */ 'localhost',
        port: process.env.MYSQL_PORT || /* istanbul ignore next */ '3306',
        user: process.env.MYSQL_USER || /* istanbul ignore next */ 'root',
        password: process.env.MYSQL_PASSWORD || /* istanbul ignore next */ '',
        database: process.env.MYSQL_DATABASE || /* istanbul ignore next */ ''
    },
{{/if_eq}}
    pool: {
        min: process.env.KNEX_POOL_MIN || /* istanbul ignore next */ 2,
        max: process.env.KNEX_POOL_MAX || /* istanbul ignore next */ 8
    },
    migrations: {
{{#if_eq databaseEngine "crdb"}}
        disableTransactions: true,  // https://github.com/knex/knex/issues/2002
{{/if_eq}}
        tableName: '{{projectName}}_knex_migrations'
    }
};
