const { ServiceBroker } = require('moleculer');
const TestService = require('../../services/{{serviceName}}.service');
const {{modelName}} = require('../../models/{{modelName}}');

describe('Test "{{serviceName}}" service', () => {
    const broker = new ServiceBroker({ logger: false });
    broker.createService(TestService);

    const knexfile = require('../../knexfile');
    const knex = require('knex')(knexfile);

    const test_object = { id: null, name: 'Test object' };

    beforeAll(async () => {
        return knex.migrate.latest()
            .then(() => broker.start());
    });

    afterAll(async () => {
        return knex.migrate.rollback()
            .then(() => knex.destroy())
            .then(() => broker.stop());
    });

    describe('Test "{{serviceName}}.create" action', () => {
        it('should return a new object with an ID', async () => {
            const res = await broker.call('{{serviceName}}.create', { name: test_object.name });
            expect(res.name).toBe('Test object');
            expect(res.id).not.toBeNull();
            test_object.id = res.id;
        });

        it('should set created_at and updated_at', async () => {
            const res = await broker.call('{{serviceName}}.create', { name: 'Test object 2' });
            expect(res.created_at).not.toBeNull();
            expect(res.updated_at).not.toBeNull();
            expect(Date.now() - (new Date(res.created_at).getTime())).toBeLessThan(1000);
            expect(Date.now() - (new Date(res.updated_at).getTime())).toBeLessThan(1000);
        });
    });

    describe('Test "{{serviceName}}.get" action', () => {
        it('should return an object', async () => {
            const res = await broker.call('{{serviceName}}.get', { id: test_object.id });
            expect(res).toBeInstanceOf({{modelName}});
            expect(res.id).toBe(test_object.id);
            expect(res.name).toBe(test_object.name);
        });

        it('should return undefined on an object not found', async () => {
            const res = await broker.call('{{serviceName}}.get', { id: '424242' });
            expect(res).not.toBeInstanceOf({{modelName}});
            expect(res).toBeUndefined();
        });
    });

    describe('Test "{{serviceName}}.list" action', () => {
        it('should return a list of {{serviceName}}', async () => {
            const res = await broker.call('{{serviceName}}.list');
            expect(res).toEqual(
                expect.arrayContaining([
                    expect.objectContaining({ name: test_object.name }),
                    expect.objectContaining({ name: 'Test object 2' })
                ])
            );
        });
    });

    describe('Test "{{serviceName}}.update" action', () => {
        it('should update the properties given', async () => {
            const res = await broker.call('{{serviceName}}.update', { id: test_object.id, name: 'Renamed object' });
            expect(res.name).toBe('Renamed object');
        });

        it('should set updated_at on update', async () => {
            const res = await broker.call('{{serviceName}}.update', { id: test_object.id, name: 'Renamed object' });
            expect(res.updated_at).not.toBeNull();
            expect(Date.now() - (new Date(res.updated_at).getTime())).toBeLessThan(1000);
            expect(res.updated_at).not.toBe(res.created_at);
        });
    });

    describe('Test "{{serviceName}}.delete" action', () => {
        it('should delete an object', async () => {
            await broker.call('{{serviceName}}.delete', { id: test_object.id });
            const res = await broker.call('{{serviceName}}.get', { id: test_object.id });
            expect(res).not.toBeInstanceOf(Object);
        });
    });
});
