exports.seed = (knex) => {
    // Deletes ALL existing entries
    return knex('{{serviceName}}').del()
        .then(() => {
            // Inserts seed entries
            return knex('{{serviceName}}').insert([
                { name: 'First seed' }
            ]);
        });
};
