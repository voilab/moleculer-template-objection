/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

const validate = {
    id: 'string',
    name: 'string'
};

module.exports = {
    name: '{{serviceName}}',

    /**
     * Settings
     */
    settings: {
        validate
    },

    /**
     * Dependencies
     */
    dependencies: [],

    /**
     * Actions
     */
    actions: {
        /**
         * Create a new {{modelName}}
         */
        create: {
            params: {
                name: validate.name
            },

            async handler(ctx) {
                return this.{{modelName}}.query()
                    .insert({
                        name: ctx.params.name
                    })
                    .returning('*');
            }
        },

        /**
         * Get a {{modelName}}
         */
        get: {
            params: {
                id: validate.id
            },

            async handler(ctx) {
                return this.{{modelName}}.query()
                    .findById(ctx.params.id);
            }
        },

        /**
         * List all {{modelName}}
         */
        async list() {
            return this.{{modelName}}.query();
        },

        /**
         * Update a {{modelName}}
         */
        update: {
            params: {
                id: validate.id,
                name: validate.name
            },

            async handler(ctx) {
                const id = ctx.params.id;
                delete ctx.params.id;

                return this.{{modelName}}.query()
                    .findById(id)
                    .patch(ctx.params)
                    .returning('*');
            }
        },

        /**
         * Delete a {{modelName}}
         */
        delete: {
            params: {
                id: validate.id
            },

            async handler(ctx) {
                return this.{{modelName}}.query()
                    .deleteById(ctx.params.id);
            }
        }
    },

    /**
     * Events
     */
    events: {

    },

    /**
     * Methods
     */
    methods: {

    },

    /**
     * Service created lifecycle event handler
     */
    created() {
        const { Model } = require('objection');

        this.{{modelName}} = require('../models/{{modelName}}');

        const knexfile = require('../knexfile');
        this.knex = require('knex')(knexfile);

        Model.knex(this.knex);
    },

    /**
     * Service started lifecycle event handler
     */
    async started() {
        return this.knex.migrate.latest();
    },

    /**
     * Service stopped lifecycle event handler
     */
    async stopped() {
        return this.knex.destroy();
    }
};
