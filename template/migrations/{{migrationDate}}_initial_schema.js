module.exports = {
    up(knex) {
        return knex.schema
            .createTable('{{serviceName}}', table => {
                table.increments('id');
                table.string('name').notNullable();
                // Enable timestamps: http://knexjs.org/#Schema-timestamps
                table.timestamps(false, true);
            });
    },

    down(knex) {
        return knex.schema
            .dropTable('{{serviceName}}');
    }
};
