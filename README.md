# Moleculer template: `objection`
:mortar_board: Moleculer-based microservice template including [Objection.js](https://vincit.github.io/objection.js/) ORM and [knex.js](http://knexjs.org/) migrations.

## Features
- Moleculer v0.14 with full-detailed `moleculer.config.js` file.
- Sample [Objection.js](https://vincit.github.io/objection.js/) model.
- Sample [knex.js](http://knexjs.org/) migration.
- Metrics & Tracing.
- Dockerfile.
- Root docker-compose.yml injection for monorepos.
- Unit tests with [Jest](http://facebook.github.io/jest/).
- Lint with [ESLint](http://eslint.org/).

## Install
To install use the [moleculer-cli](https://github.com/moleculerjs/moleculer-cli) tool.

```bash
$ moleculer alias-template objection bitbucket:voilab/moleculer-template-objection
$ moleculer init objection my-project
```

## NPM scripts
- `npm run dev`: Start development mode (load all services locally without transporter with hot-reload & REPL)
- `npm run start`: Start production mode (set `SERVICES` env variable to load certain services)
- `npm run cli`: Start a CLI and connect to production. _Don't forget to set production namespace with `--ns` argument in script_
- `npm run lint`: Run ESLint
- `npm run ci`: Run continuous test mode with watching
- `npm test`: Run tests & generate coverage report

## License
moleculer-template-objection is available under the [MIT license](https://tldrlegal.com/license/mit-license).

## Contact
Copyright (c) 2020 Voilab
